# Overview
This repository stores the licenses information used by stonewave project.

# Usage
* Create a `.env` file under this folder
* Set `STONEWAVE_SRC_HOME` in `.env`, and point it to the stonewave project home dir.
    ```
    STONEWAVE_SRC_HOME=~/path/to/stonewave
    ```
* Install `leaphopper` (https://github.com/niyue/leafhopper)
  * `pip install leafhopper`
  * Setup github personal token to avoid github rate limit (https://github.com/niyue/leafhopper#github-api-rating-limit)
* Update `extra.json` if needed
* Run `just` to generate all licenses information
  * The `*-deps.md` can be used for legal review (simply copy paste to confluence should work)
  * The `LICENSES.txt` can be used for bundling with the stonewave image for legal compilance
* Create a public snippet under `https://gitlab.com/yanhuangdata/stonewave-open-source-licenses`
* Update the link in the `Dockerfile` to download this new snippet
  * Using this approach avoids putting this big file into the source code repo

# History
You can find all historical licenses information [here](https://gitlab.com/yanhuangdata/stonewave-open-source-licenses/-/snippets/)
