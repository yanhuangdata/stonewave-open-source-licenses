#!/usr/bin/env just --justfile

set dotenv-load := true

vcpkg_json := '${STONEWAVE_SRC_HOME}/ci/dev_image/vcpkg_registry/vcpkg.json'
java_pom := '${STONEWAVE_SRC_HOME}/extensions/java_table_funcs/pom.xml'
py_toml := '${STONEWAVE_SRC_HOME}/extensions/py_table_funcs/pyproject.toml'
assets_dir := './assets'

# generate all licenses files and combine them into single one
default: py java vcpkg concat
  
# generate combined license file for python table functions
py:
  just leafhopper_gen {{py_toml}} py

# generate combined license file for java table functions
java:
  just leafhopper_gen {{java_pom}} java

# generate combined license file for vcpkg
vcpkg:
  just leafhopper_gen {{vcpkg_json}} vcpkg

# concatenate all licenses into single one
concat:
  cat {{assets_dir}}/LICENSES.*.txt > {{assets_dir}}/LICENSES.txt

# generate license file using leafhopper
leafhopper_gen project_descriptor dest_file:
  mkdir -p {{assets_dir}}
  leafhopper {{project_descriptor}} --extra=./extra.json --combine --logging-level=debug --output={{assets_dir}}/{{dest_file}}-deps.md
  mv LICENSES.txt {{assets_dir}}/LICENSES.{{dest_file}}.txt
